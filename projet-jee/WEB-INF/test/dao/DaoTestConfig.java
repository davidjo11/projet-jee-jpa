package dao;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DaoTestConfig {

	public EntityManager getTestEntityManager() {
		Map<String, String> configuration = new HashMap<>();
		configuration.put("javax.persistence.provider", "org.hibernate.jpa.HibernatePersistenceProvider");
		configuration.put("connection.driver_class",  "org.h2.Driver");
		configuration.put("hibernate.connection.url", "jdbc:h2:mem:db");
		configuration.put("hibernate.dialect",        "org.hibernate.dialect.H2Dialect");
		configuration.put("hibernate.hbm2ddl.auto",   "create-drop");
		configuration.put("hibernate.show_sql",       "true");

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myapp", configuration);
		return emf.createEntityManager();
	}
	
}
