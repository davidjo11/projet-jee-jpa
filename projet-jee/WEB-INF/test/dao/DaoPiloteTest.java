package dao;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import jpa.PiloteDaoJPA;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import api.model.Pilote;

public class DaoPiloteTest {
	
	static EntityManager em;
	
	@BeforeClass
	public static void setupEntityManager() {
		em = new DaoTestConfig().getTestEntityManager();
	}
	
	PiloteDaoJPA dao;
	
	@Before
	public void initDao() {
		dao = new PiloteDaoJPA();
		dao.em = em;
		
		em.getTransaction().begin();
		em.createQuery("delete from Pilote").executeUpdate();
	}
	
	@After
	public void closeTransaction() {
		em.getTransaction().commit();
	}
	
	@Test
	public void testCreate() {
		Pilote pilot = dao.create("Guillaume", "Dufrene");
		assertEquals("Guillaume", pilot.prenom);
		assertEquals("Dufrene", pilot.nom);
		assertTrue("Pilote.id should be set", pilot.id > 0);
	}
	
	@Test
	public void testListAll() {
		dao.create("Bruno", "Cathala");
		dao.create("Guillaume", "Dufrene");
		dao.create("Antoine", "Bauza");
		
		List<String> expectedNames = Arrays.asList( "Bauza", "Cathala", "Dufrene" );
		List<Pilote> pilots = dao.listAll();
		int len = expectedNames.size();
		assertEquals(len, pilots.size());
		for ( int i = 0; i < len; i++) {
			assertEquals("should order by lastname", expectedNames.get(i), pilots.get(i).nom);
		}
	}
	
	@Test
	public void testFind() throws Exception {
		assertNull( dao.find(9999) );
		long id = dao.create("Guillaume", "Dufrene").id;
		Pilote pilot = dao.find(id);
		assertEquals("Guillaume", pilot.prenom);
		assertEquals("Dufrene", pilot.nom);
	}
	
	@Test
	public void testUpdate() throws Exception {
		Pilote pilot = dao.create("Guillaume", "Dufrene");
		pilot.prenom = "Maxime";
		dao.update(pilot);
		
		Pilote updated = dao.find(pilot.id);
		assertEquals("Maxime", updated.prenom);
		assertEquals("Dufrene", updated.nom);
	}
	
	@Test
	public void testDelete() throws Exception {
		long id = dao.create("Guillaume", "Dufrene").id;
		Pilote existing = dao.find(id);
		assertNotNull(existing);
		
		boolean ok = dao.delete(id);
		assertTrue(ok);
		
		Pilote deleted = dao.find(id);
		assertNull(deleted);
	}
	
	@Test
	public void testDeleteAll() throws Exception {
		dao.create("Bruno", "Cathala");
		dao.create("Guillaume", "Dufrene");
		dao.create("Antoine", "Bauza");
		
		assertEquals( 3, dao.listAll().size() );
		dao.deleteAll();
		assertEquals( 0, dao.listAll().size() );
	}

}
