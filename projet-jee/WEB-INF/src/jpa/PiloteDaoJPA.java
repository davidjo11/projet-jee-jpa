package jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import api.dao.PiloteDao;
import api.model.Pilote;


public class PiloteDaoJPA implements PiloteDao {

	public EntityManager em;

	/* (non-Javadoc)
	 * @see jpa.PiloteDao#create(java.lang.String, java.lang.String)
	 */
	@Override
	public Pilote create(String firstName, String lastName) {
		// TODO : implement this.
		Pilote p = new Pilote(lastName, firstName);

		p.prenom = firstName;
		p.nom = lastName;

		em.persist(p);

		return p;
	}

	/* (non-Javadoc)
	 * @see jpa.PiloteDao#listAll()
	 */
	@Override
	public List<Pilote> listAll() {
		// TODO : implement this.
		TypedQuery<Pilote> query = em.createQuery("from Pilote order by nom", Pilote.class);
		return query.getResultList();
	}

	/* (non-Javadoc)
	 * @see jpa.PiloteDao#find(long)
	 */
	@Override
	public Pilote find(long id) {
		// TODO : implement this.
		return em.find(Pilote.class, id);
	}

	/* (non-Javadoc)
	 * @see jpa.PiloteDao#update(api.model.Pilot)
	 */
	@Override
	public boolean update( Pilote p ) {
		// TODO : implement this.
		Pilote pilot = em.find(Pilote.class, p.id);

		if(pilot == null)
			return false;

		em.merge(p);
		return true;
	}

	/* (non-Javadoc)
	 * @see jpa.PiloteDao#delete(long)
	 */
	@Override
	public boolean delete( long id ) {
		// TODO : implement this.
		Pilote p = em.find(Pilote.class, id);

		if (p == null) return false;

		em.remove(p);

		return true;
	}

	/* (non-Javadoc)
	 * @see jpa.PiloteDao#deleteAll()
	 */
	@Override
	public boolean deleteAll( ) {
		// TODO : implement this.
		em.createQuery("delete from Pilote").executeUpdate();
		return true;	
	}

}
