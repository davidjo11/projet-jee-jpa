package api.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Vol {

	@ManyToOne
	Epreuve epreuve;
	
	@ManyToOne
	Pilote pilote;

	String heureDecollage;

	String heureAtterrissage;

	Date date;
	
	@GeneratedValue
	@Id
	long id;

	public Vol(){}
}

