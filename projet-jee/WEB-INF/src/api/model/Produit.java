package api.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Produit {

	@Id
	long id;
	
	String libelle;

	double prixUnitaire;

	@ManyToOne
	Categorie category;

	public Produit(){}
}
