package api.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PointPassage {
	
	@Id
	private long id;
	
//	@ManyToMany
//	private List<Epreuve> epreuve;

	String nom, description;

	double latitude, longitude;

	Status status;

	Type type;

	public PointPassage(String nom, double latitude, double longitude, Status st){
//		this.epreuve = new ArrayList<Epreuve>();
		this.status = st;
		this.latitude = latitude;
		this.longitude = longitude;
		this.nom = nom;
	}

	public static void main(String args[]){

	}
}
