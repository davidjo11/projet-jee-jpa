package api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Profil {

	@Id
	@GeneratedValue
	long id;
	
	@OneToMany
	List<Utilisateur> users;

	String nom;

	public Profil(){}
}
