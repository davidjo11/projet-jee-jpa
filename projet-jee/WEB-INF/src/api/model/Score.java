package api.model;

import javax.persistence.*;

@Entity
public class Score {

	@Id
	@GeneratedValue
	long id;
	
	int score;
	
	int temps;
	
	@ManyToOne
	Pilote pilote;
	
	@ManyToOne
	Journee journee;
	
	public Score(){}
}
