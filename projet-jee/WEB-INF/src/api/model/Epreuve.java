package api.model;

import java.util.List;

import javax.persistence.*;

@Entity
public class Epreuve {
	
	@Id
	int id;

	@OneToMany
	List<PointPassage> point;
	
//	@ManyToMany
//	private List<PointPassage> points;

//	@OneToMany
//	private List<Vol> vols;
	
	String jour;
	
	Status status;
	
	Type type;

	@OneToOne
	Classement classement;
	
	public Epreuve(){}

}
