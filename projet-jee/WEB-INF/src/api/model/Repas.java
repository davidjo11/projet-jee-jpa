package api.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Repas extends Produit{
	
	@ManyToMany
	@OneToMany
	List<Pilote> pilotes;
	
	Date date;

	String heure;

	String menu;

	public Repas(){}
}
