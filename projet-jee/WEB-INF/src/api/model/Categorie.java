package api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Categorie {

	@Id
	long id;
	
	String titre;

	@OneToMany
	List<Produit> produits;

	public Categorie(){}
	
}
