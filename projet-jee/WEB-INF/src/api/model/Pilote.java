package api.model;

import java.util.List;

import javax.persistence.*;

@Entity
public class Pilote {

	@GeneratedValue
	@Id
	public long id ;
	public String nom, prenom, adresse, codePostal, ville, dateNaissance;	
	public int telPortable;

	@ManyToOne
	public Repas r;

//	@ManyToMany
//	public List<Repas> repas;

	@OneToMany
	public List<Vol> vols;

	@ManyToMany
	List<Competition> competitions;
	
	@OneToMany
	List<Score> scores;
	
	public Pilote(String nom, String prenom){
		this.nom = nom;
		this.prenom = prenom;
	}


	public static void main(String args[]){

	}
}