package api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Club {

	@Id
	private long id;
	
	String nom;
	String ville;

	@OneToMany
	List<Pilote> pilotes;

	@ManyToOne
	Region region;
	
	public Club(String nom){
		this.nom = nom;
	}

	public void addPilote(Pilote p){
		this.pilotes.add(p);
	}

	public static void main(String args[]){

	}
}
