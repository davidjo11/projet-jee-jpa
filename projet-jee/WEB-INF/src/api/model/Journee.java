package api.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Journee {

	@GeneratedValue
	@Id
	long id;
	
	@OneToMany
	List<Epreuve> epreuves;
	
	@OneToMany
	List<Pilote> pilotes;

	Date date;
	
	Status status;
	
	@OneToMany
	List<Score> scores;
	
	public Journee(){
		
	}
	
	public Journee(Date d){
		this.date = d;
	}
	
	public Journee(Date date, List<Pilote> pilotes, List<Epreuve> epreuves){
		this.date = date;
		this.epreuves = epreuves;
		this.pilotes = pilotes;
	}
	
}
