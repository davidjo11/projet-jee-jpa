package api.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Utilisateur {

	@Id
	private long id;
	
	@ManyToOne
	Profil profil;

	String identifiant;

	String motDePasse;

	String nom, prenom;

	public Utilisateur(){}
}
