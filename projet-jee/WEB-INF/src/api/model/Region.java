package api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Region {

	@Id
	private long id;
	
	@OneToMany
	List<Club> clubs;

	String nom;

	public Region(){}
}
