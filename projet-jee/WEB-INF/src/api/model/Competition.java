package api.model;

import java.util.List;

import javax.persistence.*;

@Entity
public class Competition {

	@OneToMany
	List<Journee> journees;
	
	@ManyToMany
	List<Pilote> pilotes;
	
	@OneToOne
	Classement classement;
	
	Status status;
	
	@ManyToOne
	Pilote vainqueur;
	
	public Competition(){
		
	}
}
