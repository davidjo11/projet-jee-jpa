package api.dao;

import java.util.List;

import api.model.Pilote;

public interface PiloteDao {

	/**
	 * Create a new Pilote
	 * @param firstName
	 * @param lastName
	 * @return created Pilote with ID
	 */
	public abstract Pilote create(String firstName, String lastName);

	/**
	 * get all pilots ordered by lastname, firstname
	 * @return pilots
	 */
	public abstract List<Pilote> listAll();

	/**
	 * Get an pilot by its ID
	 * @param id
	 * @return pilot
	 */
	public abstract Pilote find(long id);

	/**
	 * Update pilot firstname and/or lastname
	 * @param pilot
	 * @return true if succeed
	 */
	public abstract boolean update(Pilote pilot);

	/**
	 * Delete an pilot by its ID
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all pilots
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();

}