package api.dao;

import java.util.*;

import api.model.*;

public interface EpreuveDao {

	/**
	 * Create a new Epreuve
	 * @param nom
	 * @param date
	 * @param pilotes
	 * @return created Epreuve with ID
	 */
	public abstract Epreuve create(String nom, Date d, List<Pilote> pilotes);

	/**
	 * get all epreuves
	 * @return epreuves
	 */
	public abstract List<Epreuve> listAll();

	/**
	 * Get a epreuve by its ID
	 * @param id
	 * @return epreuve
	 */
	public abstract Epreuve find(long id);

	/**
	 * Update chart's pilot's or date or name
	 * @param epreuve
	 * @return true if succeed
	 */
	public abstract boolean update(Epreuve epreuve);

	/**
	 * Delete a epreuve by Id 
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all epreuves
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();
}
