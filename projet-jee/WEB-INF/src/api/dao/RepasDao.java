package api.dao;

import java.util.List;

import api.model.*;

public interface RepasDao {

	/**
	 * Create a new Repas
	 * @param scores
	 * @return created Repas with ID
	 */
	public abstract Classement create(String libelle, double price);

	/**
	 * get all repas 
	 * @return repas
	 */
	public abstract List<Classement> listAll();

	/**
	 * Get a repas by its ID
	 * @param id
	 * @return repas
	 */
	public abstract Classement find(long id);

	/**
	 * Update repas' name, menu, libelle, price
	 * @param repas
	 * @return true if succeed
	 */
	public abstract boolean update(Repas repas);

	/**
	 * Delete a repas by Id 
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all repas
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();
}
