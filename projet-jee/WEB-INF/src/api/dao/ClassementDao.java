package api.dao;

import java.util.List;

import api.model.*;

public interface ClassementDao {

	/**
	 * Create a new Classement
	 * @param scores
	 * @return created Classement with ID
	 */
	public abstract Classement create(List<Score> scores);

	/**
	 * get all charts 
	 * @return charts
	 */
	public abstract List<Classement> listAll();

	/**
	 * Get a chart by its ID
	 * @param id
	 * @return chart
	 */
	public abstract Classement find(long id);

	/**
	 * Update chart's pilot's score or position
	 * @param chart
	 * @return true if succeed
	 */
	public abstract boolean update(Classement chart);

	/**
	 * Delete a chart by Id 
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all charts
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();
	
}
