package api.dao;

import java.util.List;

import api.model.*;

public interface CompetitionDao{


	/**
	 * Create a new competition
	 * @param name
	 * @return created Competition with ID
	 */
	public abstract Pilote create(String name);

	/**
	 * get all competitions
	 * @return competitions
	 */
	public abstract List<Competition> listAll();

	/**
	 * Get a competition by its ID
	 * @param id
	 * @return competition
	 */
	public abstract Club find(long id);
	
	/**
	 * Update competition
	 * @param competition
	 * @return true if succeed
	 */
	public abstract boolean update(Competition competition);

	/**
	 * Delete a competition by Id 
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all competition
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();
}
