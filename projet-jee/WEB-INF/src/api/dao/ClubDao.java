package api.dao;

import java.util.List;

import api.model.*;

public interface ClubDao {

	/**
	 * Create a new Club
	 * @param name
	 * @return created Club with ID
	 */
	public abstract Pilote create(String name);

	/**
	 * get all clubs
	 * @return clubs
	 */
	public abstract List<Club> listAll();

	/**
	 * Get a club by its ID
	 * @param id
	 * @return club
	 */
	public abstract Club find(long id);
	
	/**
	 * Update club 
	 * @param day
	 * @return true if succeed
	 */
	public abstract boolean update(Club club);

	/**
	 * Delete a club by Id 
	 * @param id
	 * @return true if succeed
	 */
	public abstract boolean delete(long id);

	/**
	 * delete all clubs
	 * @return true if succeed
	 */
	public abstract boolean deleteAll();
}
