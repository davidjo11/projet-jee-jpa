package api.dao;

import java.util.List;

import api.model.*;

public interface ScoreDao {

		/**
		 * Create a new Score
		 * @param name
		 * @return created Score with ID
		 */
		public abstract Score create(Pilote pilote, int points);

		/**
		 * get all scores
		 * @return scores
		 */
		public abstract List<Score> listAll();

		/**
		 * Get a score by its ID
		 * @param id
		 * @return score
		 */
		public abstract Score find(long id);
		
		/**
		 * Update score
		 * @param score
		 * @return true if succeed
		 */
		public abstract boolean update(Score score);

		/**
		 * Delete a score by Id 
		 * @param id
		 * @return true if succeed
		 */
		public abstract boolean delete(long id);

		/**
		 * delete all Score
		 * @return true if succeed
		 */
		public abstract boolean deleteAll();
}
