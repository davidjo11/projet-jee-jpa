package impl;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import api.dao.PiloteDao;
import api.model.Pilote;

@Controller
public class PiloteControl {

	PiloteDao piloteDao;
	
	@RequestMapping("/listPilots.html")
	public String listAll(Model model){
		List<Pilote> l = piloteDao.listAll();
		model.addAttribute("pilotes", l);
		return "PiloteList";
	}
}
